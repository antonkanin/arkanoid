﻿using UnityEngine;

public abstract class ABonusButtonAsset : ScriptableObject
{
    [SerializeField] private float m_cooldown;

    [SerializeField] private float m_duration;
    [SerializeField] private Sprite m_icon;
	[SerializeField] private int m_price;

	private float m_currentCoolDown = 0.0f;

	public Sprite Icon
	{
		get { return m_icon; }
	}

	public int Price
	{
		get { return m_price; }
	}

	public float Duration
	{
		get { return m_duration; }
	}

	public float Cooldown
	{
		get { return m_currentCoolDown; }
	}

	public virtual void Activate()
	{
		m_currentCoolDown = m_cooldown;
	}

	public float CooldownNormalized
	{
		get { return Mathf.Max(0, m_currentCoolDown / m_cooldown); }
	}

	public void UpdateCooldown(float dt)
	{
		m_currentCoolDown = Mathf.Max(0, m_currentCoolDown - dt);
	}
}
