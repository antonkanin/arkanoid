﻿using UnityEngine;

[CreateAssetMenu(menuName = "BonusButtonAsset/Ball", fileName = "NewBonusBallAsset")]
public class BallBonusButtonAsset : ABonusButtonAsset
{
	[SerializeField] private BallType m_type = BallType.Base;

	public override void Activate()
	{
		base.Activate();

		var platform = GameObject.FindGameObjectWithTag("Platform");
		if (platform == null)
		{
			Debug.LogWarning("There is no platform in the scene");
			return;
		}

	    var bonus = platform.AddComponent<BonusBall>();
	    bonus.Duration = Duration;
	    bonus.BallType = m_type;
	}
}
