﻿using UnityEngine;

public class BallistaAiming : MonoBehaviour
{
	[Header("Reference:")]
	[SerializeField] private Transform m_tower = null;

	[Header("Settings:")]
	[SerializeField] private float m_rotationSpeed = 1.0f;

	private GameObject m_target;

	public GameObject Target
	{
		get { return m_target; }
		set	{ m_target = value;	}
	}

	private void Update()
	{
		if (Target == null)
		{
			FindClosestEnemyTarget();
			return;
		}

		RotateTower(m_tower);
	}

	private void RotateTower(Transform tower)
	{
		var lookDir = Target.transform.position - tower.position;
		lookDir.y = 0;

		var targetRot = Quaternion.LookRotation(lookDir);
		var rotSpeed = m_rotationSpeed * Time.deltaTime;
		m_tower.rotation = Quaternion.Lerp(m_tower.rotation, targetRot, rotSpeed);
	}

	private void FindClosestEnemyTarget()
	{
		var enemies = GameObject.FindGameObjectsWithTag("Enemy");

		if (enemies.Length == 0)
		{
			return;
		}

		var smallestDist = float.MaxValue;

		foreach (var enemy in enemies)
		{
			var dist = (enemy.transform.position - m_tower.position).sqrMagnitude;
			if (dist < smallestDist)
			{
				Target = enemy;
				smallestDist = dist;
			}
		}
	}
}
