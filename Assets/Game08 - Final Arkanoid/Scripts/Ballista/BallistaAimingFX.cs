﻿using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallistaAimingFX : MonoBehaviour
{
	[SerializeField] private Transform m_aimFX = null;
	[SerializeField] private float m_movementSpeed = 5.0f;

	private BallistaAiming m_aiming;

	private void Start()
	{
		m_aiming = GetComponent<BallistaAiming>();
	}

	private void Update()
	{
		var targetExist = (m_aiming.Target != null);

		m_aimFX.gameObject.SetActive(targetExist);

		if (!targetExist)
		{
			return;
		}

		var movementSpeed = m_movementSpeed * Time.deltaTime;
		var newPosition = Vector3.Lerp(m_aimFX.position, m_aiming.Target.transform.position, movementSpeed);

		newPosition.y = m_aimFX.position.y;

		m_aimFX.position = newPosition;
	}
}
