﻿using System;
using UnityEngine;

[RequireComponent(typeof(BallistaAiming))]
public class BallistaShooting : MonoBehaviour
{
	[Header("Referneces")]
	[SerializeField] private GameObject m_ammo = null;
	[SerializeField] private Transform m_firePoint = null;

	[Header("Settings:")]
	[SerializeField] private float m_shotForce = 300f;
	[SerializeField] private float m_shootingInterval = 1f;
	[SerializeField] private LayerMask m_shootingMask;

	private BallistaAiming m_aiming;
	private float m_shootingTimer;

	private void Start()
	{
		m_aiming = GetComponent<BallistaAiming>();
	}

	private void Update()
	{
		UpdateTimer();		

		if (m_aiming.Target == null)
		{
			return;
		}

		if (m_shootingTimer <= 0)
		{
			// TODO protect platform

			MakeShot();
			ResetTimer();
		}
	}

	private void ResetTimer()
	{
		m_shootingTimer = m_shootingInterval;
	}

	private void MakeShot()
	{
		// TODO PlayAnim

		var shotPos = m_firePoint.position;
		var shotRot = m_firePoint.rotation;

		var ammoClone = Instantiate(m_ammo, shotPos, shotRot);
		var rb = ammoClone.GetComponent<Rigidbody>();

		rb.isKinematic = false;
		rb.AddForce(m_firePoint.forward * m_shotForce, ForceMode.Impulse);
	}

	private void UpdateTimer()
	{
		m_shootingTimer -= Time.deltaTime;
	}
}
