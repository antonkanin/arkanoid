﻿using UnityEngine;

public class BallTypeBonusActivator : ABonusActivator
{
    [SerializeField] private BallType m_ballType;

    public override void Activate(GameObject target)
    {
        var bonus = target.GetComponent<BonusBall>();
        if (bonus == null)
        {
            bonus = target.AddComponent<BonusBall>();
            bonus.NoTimeLimit = m_noTimeLimit;
        }
    }
}
