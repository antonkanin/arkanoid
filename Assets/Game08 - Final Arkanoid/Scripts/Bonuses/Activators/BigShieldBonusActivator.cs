﻿using UnityEngine;

public class BigShieldBonusActivator : ABonusActivator
{
	public override void Activate(GameObject target)
	{
	    var bonus = target.GetComponent<BonusBigShield>();
	    if (bonus == null)
	    {
	        bonus = target.AddComponent<BonusBigShield>();
	        bonus.NoTimeLimit = m_noTimeLimit;
	    }
	    bonus.Duration = m_duration;
	}
}