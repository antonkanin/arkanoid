﻿using UnityEngine;

public class MovementBonusActivator : ABonusActivator
{
	[SerializeField] private float m_increaseSpeedBy = 0;
	[SerializeField] private FloatVariable m_initialPlatformSpeed = null;
	[SerializeField] private FloatVariable m_currentPlatformSpeed = null;

	public override void Activate(GameObject target)
	{
		var bonus = target.AddComponent<BonusPlatformMovement>();

		bonus.Duration = m_duration;
	    bonus.NoTimeLimit = m_noTimeLimit;
		bonus.IncreaseSpeedBy = m_increaseSpeedBy;
		bonus.PlatformInitialSpeed = m_initialPlatformSpeed;
		bonus.PlatformCurrentSpeed = m_currentPlatformSpeed;
	}
}
