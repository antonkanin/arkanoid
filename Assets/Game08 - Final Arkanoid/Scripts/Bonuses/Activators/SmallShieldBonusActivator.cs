﻿using UnityEngine;

public class SmallShieldBonusActivator : ABonusActivator
{
	public override void Activate(GameObject target)
	{
	    var bonus = target.GetComponent<BonusBigShield>();
	    if (bonus != null)
	    {
	        Destroy(bonus);
        }
	}
}