﻿using UnityEngine;

public class StarBonusActivator : ABonusActivator
{
    public override void Activate(GameObject target)
    {
        GameController.StarsCount++;
        Debug.Log("Death to the great star!");
        Destroy(gameObject);
    }
}
