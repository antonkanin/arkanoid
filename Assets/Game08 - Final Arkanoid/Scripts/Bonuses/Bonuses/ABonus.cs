﻿using UnityEngine;

public abstract class ABonus : MonoBehaviour
{
	public float Duration { get; set; }

    public bool NoTimeLimit { get; set; }

    protected abstract void Activate();
	protected abstract void Deactivate();

    private void Awake()
    {
        NoTimeLimit = true;
    }

	private void Start()
	{
		Activate();
	}

	private void OnDestroy()
	{
		Deactivate();
	}

	private void Update()
	{
	    if (NoTimeLimit == false)
	    {
	        Duration -= Time.deltaTime;
	        if (Duration <= 0)
	        {
	            Destroy(this);
	        }
        }
    }
}
