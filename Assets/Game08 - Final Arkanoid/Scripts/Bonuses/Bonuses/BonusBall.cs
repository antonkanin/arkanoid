﻿using UnityEngine;

public class BonusBall : ABonus
{
    public BallType BallType { get; set; }

    private GameObject m_ballObject;

    protected override void Activate()
    {
        var balls = GameObject.FindGameObjectsWithTag("Ball");

        foreach (var ball in balls)
        {
            var ballBehaviourExecutor = ball.GetComponent<BallBehaviourExecutor>();
            ballBehaviourExecutor.SetBallType(BallType);
        }
    }

    protected override void Deactivate()
    {
        if (m_ballObject == null)
        {
            Debug.Log("m_ballObject was not assigned in " + gameObject.name);
        }
        else
        {
            var ballBehaviourExecutor = m_ballObject.GetComponent<BallBehaviourExecutor>();

            if (ballBehaviourExecutor != null)
            {
                ballBehaviourExecutor.SetBallType(BallType.Base);
            }
        }
    }
}
