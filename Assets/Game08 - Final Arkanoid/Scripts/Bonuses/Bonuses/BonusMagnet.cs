﻿using UnityEngine;

public class BonusMagnet : MonoBehaviour
{
    [SerializeField] private float m_squareDistance = 10;
    [SerializeField] private float m_magnetForce = 3000;

    private GameObject[] balls;

    // Update is called once per frame
    void Update()
    {
        balls = GameObject.FindGameObjectsWithTag("Ball");
        foreach (var ball in balls)
        {
            var ballDirection = transform.position - ball.transform.position;
            if (ballDirection.sqrMagnitude <= m_squareDistance)
            {
                ball.GetComponent<Rigidbody>().AddForce(ballDirection.normalized * m_magnetForce);
            }
        }
    }
}
