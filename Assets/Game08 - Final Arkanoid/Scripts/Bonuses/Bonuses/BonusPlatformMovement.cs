﻿using UnityEngine;

public class BonusPlatformMovement : ABonus
{
	public FloatVariable PlatformCurrentSpeed { get; set; }
	public FloatVariable PlatformInitialSpeed { get; set; }
	public float IncreaseSpeedBy { get; set; }

	protected override void Activate()
	{
		PlatformCurrentSpeed.Value *= IncreaseSpeedBy;
	}

	protected override void Deactivate()
	{
		PlatformCurrentSpeed.Value = PlatformInitialSpeed.Value;
	}
}
