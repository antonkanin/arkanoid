﻿using UnityEngine;

[RequireComponent(typeof(ObjectHealth))]
public class EnableFire : MonoBehaviour
{
    [SerializeField] private float m_selfDamageInterval = 2.0f;
    [SerializeField] private float m_damage = 0.5f;
    [SerializeField] private float m_fireOthersDelay = 1.0f;
    [SerializeField] private float m_fireOthersRadius = 4.0f;
    [SerializeField] private GameObject m_fireFX = null;

    private bool m_isActive = false;
    private float m_damageTimer = 0.0f;

    public bool IsActive
    {
        get { return m_isActive; }
        set
        {
            m_isActive = value;
            m_fireFX.SetActive(value);
            if (value)
            {
                m_damageTimer = 0;
                SelfDamage();
            }
        }
    }

    private void Start()
    {
        IsActive = false;
    }

    private void Update()
    {
        if (IsActive)
        {
            TrySelfDamage();
            TryDamageOthers();
        }
    }

    private void TrySelfDamage()
    {
        m_damageTimer += Time.deltaTime;
        if (m_damageTimer >= m_selfDamageInterval)
        {
            SelfDamage();
            m_damageTimer = 0;
        }
    }

    private void TryDamageOthers()
    {
        if (m_fireOthersDelay >= 0)
        {
            m_fireOthersDelay -= Time.deltaTime;
        }
        else
        {
            // check with everything except layer #15 "Arkanoid_Floor"
            int layerMask = ~(1 << 15);

            var casted = Physics.OverlapSphere(transform.position, m_fireOthersRadius, layerMask);

            foreach (var hitObject in casted)
            {
                EnableFire enableFire = hitObject.gameObject.GetComponent<EnableFire>();
                if (enableFire != null && enableFire.IsActive == false)
                {
                    enableFire.IsActive = true;
                }
            }
        }
    }

    void SelfDamage()
    {
        GetComponent<ObjectHealth>().Health -= m_damage;
    } 
}
