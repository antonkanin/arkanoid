﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LogSpeed : MonoBehaviour
{
	private Rigidbody m_rb;

	void Start()
	{
		m_rb = GetComponent<Rigidbody>();
	}

	void Update()
	{
		Debug.Log("Speed: " + m_rb.velocity.magnitude);
	}
}
