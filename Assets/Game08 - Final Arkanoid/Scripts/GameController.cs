﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class IntUpdateEvent : UnityEvent<int> {}

public class GameController : MonoBehaviour
{
    public static IntUpdateEvent OnStarsCountUpdate = new IntUpdateEvent();

    public static int PlayerHealth;
	public static bool IsVictory;
	public static bool IsPaused;

	public static Action<bool> EndOfGameEvent;

	// TODO move this to CustomInput
	public delegate void GameStartEventHandler();
    public static GameStartEventHandler OnGameStartEvent;

    private static int m_StarsCount = 0;

    public static int StarsCount
    {
        get { return m_StarsCount; }
        set
        {
            m_StarsCount = value;
            OnStarsCountUpdate.Invoke(m_StarsCount);
        }
    }

    // TODO static method vs singelton vs injection?
    public static void StartGame()
    {
        if (OnGameStartEvent != null)
        {
            OnGameStartEvent.Invoke();
        }
    }

    // That's a shitty solution, need to ask sensei if we can call static methods from Unity Events
    public void StartGame2()
    {
        StartGame();
    }

	private float m_timer = 1f;

	// Use this for initialization
	private void Start()
	{
		ResetGameState();
	}

	private void Update()
	{
		if (Time.time < m_timer)
		{
			return;
		}

		m_timer = Time.time + 1f;

		var enemies = GameObject.FindGameObjectsWithTag("Enemy");
		var isEndOfGame = (PlayerHealth <= 0 || enemies.Length == 0);

		if (isEndOfGame)
		{
			if (PlayerHealth <= 0)
				IsVictory = false;
			else if (enemies.Length == 0)
				IsVictory = true;

		    TrySaveStarsCount();

            if (EndOfGameEvent != null)
			{
                EndOfGameEvent.Invoke(IsVictory);
				enabled = false;
            }
        }
	}

    private void TrySaveStarsCount()
    {
        var currentSceneNum = SceneManager.GetActiveScene().buildIndex;
        if (StarsStorage.GetStars(currentSceneNum) < StarsCount)
        {
            StarsStorage.SetStars(currentSceneNum, StarsCount);
        }
    }

	private void ResetGameState()
	{
		IsVictory = false;
		IsPaused = true;
		PlayerHealth = 5;
	}
}