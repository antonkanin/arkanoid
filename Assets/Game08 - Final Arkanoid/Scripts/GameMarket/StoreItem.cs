﻿using UnityEngine;

[CreateAssetMenu]
public class StoreItem : ScriptableObject
{
	[SerializeField] private string m_name;
	[SerializeField] private Sprite m_icon;

	[Header("Good:")]
	[SerializeField] private ECurrency m_goodCurrency;
	[SerializeField] private int m_goodAmount;

	[Header("Price:")]
	[SerializeField] private float m_price;

	public string MName
	{
		get { return m_name; }
	}

	public Sprite MIcon
	{
		get { return m_icon; }
	}

	public ECurrency MGoodCurrency
	{
		get { return m_goodCurrency; }
	}

	public int MGoodAmount
	{
		get { return m_goodAmount; }
	}

	public float MPrice
	{
		get { return m_price; }
	}

	public bool Buy()
	{
		var userValue = UserWallet.GetValue(m_goodCurrency);
		UserWallet.SetValue(m_goodCurrency, userValue + m_goodAmount);

		return true;
	}
}
