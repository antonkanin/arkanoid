﻿using System;
using UnityEngine;

public class BallAttack : MonoBehaviour
{
	[SerializeField] private float m_damage = 1f;

    [SerializeField] private bool m_isFireBall = false;

    public bool IsFireBall
    {
        get { return m_isFireBall; }
        set { m_isFireBall = value; }
    }
    
    public delegate void ObjectHitEventHandler();

    private ObjectHitEventHandler m_onObjectHitEvent;
    public event ObjectHitEventHandler OnObjectHitEvent
    {
        add
        {
            m_onObjectHitEvent += value;
            Debug.Log("Subscribed");
        }
        remove
        {
            m_onObjectHitEvent -= value;
            Debug.Log("Unsubscribed");
        }
    }

    public float Damage
	{
		get { return m_damage; }
		set { m_damage = value; }
	}

	private void OnCollisionEnter(Collision collision)
	{
		TrySetDamage(collision.gameObject);
	    if (m_isFireBall)
	    {
	        TrySetOnFire(collision.gameObject);
	    }
	}

    private void TrySetOnFire(GameObject gameObject)
    {
        EnableFire enableFire = gameObject.GetComponent<EnableFire>();

        if (enableFire != null && enableFire.IsActive == false)
        {
            enableFire.IsActive = true;
            Debug.Log("Enabling fire for: " + gameObject.name);
        }
    }

    private void TrySetDamage(GameObject gameObject)
	{
		ObjectHealth health = gameObject.GetComponent<ObjectHealth>();

		if (health != null)
		{
			health.Health -= m_damage;

		    if (m_onObjectHitEvent != null)
		    {
		        m_onObjectHitEvent.Invoke();
            }
        }
    }
}
