﻿using UnityEngine;

[CreateAssetMenu(menuName ="BallAssets/BaseBall", fileName = "NewBaseBall")]
public class BallBehaviourAsset : ScriptableObject
{
	[Header("Settings:")]
	[SerializeField] private float m_speed = 15f;
	[SerializeField] private float m_radius = 0.45f;
	[SerializeField] private float m_damage = 1f;

	public virtual void EnableBehaviour(GameObject target)
	{
		SetSpeed(target);
		SetRadius(target);
		SetDamage(target);
	}

	public virtual void DisableBehaviour(GameObject target)
	{

	}

	private void SetSpeed(GameObject target)
	{
		var ballSpeed = target.GetComponent<BallSpeed>();

		if (ballSpeed != null)
		{
			ballSpeed.CurrSpeed = m_speed;
		}
	}

	private void SetRadius(GameObject target)
	{
		var collider = target.GetComponent<SphereCollider>();

		if (collider != null)
		{
			collider.radius = m_radius;
		}
	}

	private void SetDamage(GameObject target)
	{
		var attack = target.GetComponent<BallAttack>();

		if (attack != null)
		{
			attack.Damage = m_damage;
		}
	}
}
