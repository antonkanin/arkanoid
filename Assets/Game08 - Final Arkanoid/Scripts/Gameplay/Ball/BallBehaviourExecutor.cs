﻿using UnityEngine;

public class BallBehaviourExecutor : MonoBehaviour
{
	[SerializeField] private BallType m_defaultType = BallType.Base;
	[SerializeField] private BallBehaviourAsset[] m_assets = null;
	[SerializeField] private GameObject[] m_views = null;

	private void Start()
	{
		SetBallType(m_defaultType);
	}

	public void SetBallType(BallType type)
	{
		var typeIndex = (int)type;

		for (int i = 0; i < m_views.Length; i++)
		{
			m_views[i].SetActive(typeIndex == i);

			if (i == typeIndex)
			{
				m_assets[typeIndex].EnableBehaviour(gameObject);
			}
			else
			{
				m_assets[typeIndex].DisableBehaviour(gameObject);
			}
		}
	}
}
