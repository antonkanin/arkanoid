﻿using UnityEngine;

[CreateAssetMenu(menuName = "BallAssets/BallFire", fileName = "NewBallFire")]
public class BallFireBehaviour : BallBehaviourAsset
{
    public override void EnableBehaviour(GameObject target)
    {
        base.EnableBehaviour(target);

        target.GetComponent<BallAttack>().IsFireBall = true;

        Debug.Log("Enabling FireBall behaviour");
    }

    public override void DisableBehaviour(GameObject target)
    {
        base.DisableBehaviour(target);

        target.GetComponent<BallAttack>().IsFireBall = false;

        Debug.Log("Disabling FireBall behaviour");
    }
}
