﻿using UnityEngine;

[CreateAssetMenu(menuName = "BallAssets/BallKamikaze", fileName = "NewBallKamikaze")]
public class BallKamikaze : BallBehaviourAsset
{
    public override void EnableBehaviour(GameObject target)
    {
        var ballAttack = target.GetComponent<BallAttack>();
        ballAttack.OnObjectHitEvent += ResetBall;
    }

    public override void DisableBehaviour(GameObject target)
    {
        if (target != null)
        {
            var ballAttack = target.GetComponent<BallAttack>();
            ballAttack.OnObjectHitEvent -= ResetBall;
        }
    }

    void ResetBall()
    {
        GameController.IsPaused = true;
    }
}