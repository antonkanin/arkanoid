﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Button))]
public class BonusActivateButton : MonoBehaviour
{
	//[SerializeField] private BallType m_type = BallType.Base;
	[SerializeField] private ABonusButtonAsset m_bonusAsset;

	[Header("References:")] [SerializeField]
	private Image m_iconImage;

	[SerializeField] private Image m_cooldownImage = null;
	[SerializeField] private Text m_priceText;

	private void Start()
	{
		ApplyAsset();

		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	private void Update()
	{
#if UNITY_EDITOR
		ApplyAsset();
#endif

		if (!Application.isPlaying)
		{
			return;
		}

		if (m_bonusAsset != null)
		{
			m_bonusAsset.UpdateCooldown(Time.deltaTime);
			ApplyCooldown();
		}
	}

	private void OnDestroy()
	{
		GetComponent<Button>().onClick.RemoveListener(OnClick);
	}

	private void OnClick()
	{
        var currentCoinsAmount = UserWallet.GetValue(ECurrency.Coin);

        if (m_bonusAsset != null && m_bonusAsset.CooldownNormalized <= 0 &&
            m_bonusAsset.Price <= currentCoinsAmount)
		{
			m_bonusAsset.Activate();
            UserWallet.SetValue(ECurrency.Coin, currentCoinsAmount - m_bonusAsset.Price);
        }
	}

	private void ApplyAsset()
	{
		if (m_bonusAsset == null || m_iconImage == null || m_priceText == null || m_cooldownImage == null)
		{
			return;
		}

		m_iconImage.sprite = m_bonusAsset.Icon;
		m_priceText.text = m_bonusAsset.Price.ToString();
	}

	private void ApplyCooldown()
	{
		if (m_cooldownImage == null)
		{
			Debug.Log("m_cooldownImage is not assigned for : " + gameObject.name);
		}

		m_cooldownImage.fillAmount = m_bonusAsset.CooldownNormalized;
	}
}
