﻿using System;
using UnityEngine;

public class BallShooting : MonoBehaviour
{
	[SerializeField] private GameObject m_ballPrefab = null;
	[SerializeField] private float m_ballPushForce = 100;
    [SerializeField] private Transform m_ballSpawnPlace = null;

	private Vector3 m_startPosition;

	private Rigidbody m_RigidBody;

	private GameObject m_ball;

	void Start()
	{
		m_ball = Instantiate(m_ballPrefab, m_ballSpawnPlace.position, 
			transform.rotation, transform);

		m_RigidBody = m_ball.GetComponent<Rigidbody>();
	}

    private void OnEnable()
    {
        GameController.OnGameStartEvent += LaunchBall;
    }

    private void OnDisable()
    {
        GameController.OnGameStartEvent -= LaunchBall;
    }

    void FixedUpdate()
	{
		if (GameController.IsPaused)
		{
			ResetBallOnce();

			// is the balls is still
		}
	}

    void LaunchBall()
    {
        if (m_RigidBody != null && m_RigidBody.isKinematic) // TODO do we care is the ball is already Kinematic?
        {
            TryPushBall();
        }
    }

    private void TryPushBall()
    {
        if (m_RigidBody != null)
        {
            m_RigidBody.isKinematic = false;
            m_RigidBody.transform.parent = null;
            m_RigidBody.AddForce(m_RigidBody.transform.forward * m_ballPushForce, ForceMode.Impulse);

            GameController.IsPaused = false;
        }
    }

    private void ResetBallOnce()
	{
	    if (!m_RigidBody.isKinematic)
	    {
	        m_RigidBody.isKinematic = true;
	        m_RigidBody.transform.SetParent(m_ballSpawnPlace);
	        m_RigidBody.transform.SetPositionAndRotation(m_ballSpawnPlace.position, m_ballSpawnPlace.rotation);
	    }
    }
}
