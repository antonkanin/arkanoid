﻿using UnityEngine;

public class HeadRotation : MonoBehaviour
{
	[SerializeField] private GameObject m_headObject = null;
	[SerializeField] private TargetType m_targetType = TargetType.Ball;
	[SerializeField] private float m_lerpSpeed = 1.0f;

	private GameObject m_targetObject;
	private Quaternion m_rotation;

	enum TargetType
	{
		Platform = 1,
		Ball = 2
	}



	private void LateUpdate()
	{
		if (m_targetObject == null)
		{
			var targetTag = GetTagName(m_targetType);
			m_targetObject = GameObject.FindGameObjectWithTag(targetTag);
		}

		if (m_targetObject)
		{
			var targetDirection = m_targetObject.transform.position - transform.position;
			var rot = m_headObject.transform.rotation;
			var targetRot = Quaternion.LookRotation(targetDirection);

			m_headObject.transform.rotation = Quaternion.Lerp(rot, targetRot, Time.deltaTime * m_lerpSpeed);
		}
	}

	private string GetTagName(TargetType targetType)
	{
		switch (targetType)
		{
			case TargetType.Platform:
				return "Platform";
			case TargetType.Ball:
				return "Ball";
			default:
				return "";
		}
	}
}