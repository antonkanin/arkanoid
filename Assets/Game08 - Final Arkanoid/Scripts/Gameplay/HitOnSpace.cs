﻿using UnityEngine;

public class HitOnSpace : MonoBehaviour
{
	[SerializeField] private float m_maxTime = 1f;

	[Header("References")]
	[SerializeField] private Collider[] m_colliders = null;

	private float m_timer;

	void Start()
	{
		SetEnableCollider(false);
	}

	void Update()
	{
		bool isTimerEnd = m_timer < 0;

		if (isTimerEnd && Input.GetKeyDown(KeyCode.Space))
		{
			m_timer = m_maxTime;
		}

		SetEnableCollider(!isTimerEnd);

		m_timer -= Time.deltaTime;
	}

	private void SetEnableCollider(bool enable)
	{
		foreach (var collider in m_colliders)
		{
			collider.enabled = enable;
		}
	}
}
