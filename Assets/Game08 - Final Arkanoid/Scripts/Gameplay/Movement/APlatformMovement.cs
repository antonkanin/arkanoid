﻿using UnityEngine;

public abstract class APlatformMovement : MonoBehaviour
{
    [Header("Movement")]
	[SerializeField] private float m_leftBound = -8.5f;
	[SerializeField] private float m_rightBound = 8.5f;
	[SerializeField] private float m_lerpSpeed = 8f;

	[Header("Rotation")]
	[SerializeField] private float m_rotationSmoothness = 2f;
	[SerializeField] private float m_maxAngle = 45f;

	[Header("References")]
    [SerializeField] protected FloatVariable m_movementSpeed;
    [SerializeField] private FloatVariable m_initialMovementSpeed = null;
    [SerializeField] private Animator m_animator = null;

	protected Vector3 m_currentVelocity;

	protected abstract Vector3 CalculateVelocity();

    private void Start()
	{
		m_movementSpeed.Value = m_initialMovementSpeed.Value;
	}

	private void Update()
	{
		
		var targetVelocity = CalculateVelocity();

		// TODO add custom input
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			targetVelocity = Vector3.left * m_movementSpeed.Value;
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			targetVelocity = Vector3.right * m_movementSpeed.Value;
		}


		DoMovemenet(targetVelocity);
		DoRotation(targetVelocity);

        m_animator.SetFloat("Speed", m_currentVelocity.magnitude);
	}

	private void DoMovemenet(Vector3 targetVelocity)
	{
		m_currentVelocity = Vector3.Lerp(m_currentVelocity, targetVelocity, Time.deltaTime * m_lerpSpeed);
		Vector3 newPos = transform.position + m_currentVelocity;
		newPos.x = Mathf.Clamp(newPos.x, m_leftBound, m_rightBound);
		transform.position = newPos;
	}

	private void DoRotation(Vector3 targetVelocity)
	{
		var yAngle = targetVelocity.x / m_movementSpeed.Value * m_maxAngle;
		var rot = Quaternion.Euler(0, yAngle, 0);

		transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * m_rotationSmoothness);
	}
}
