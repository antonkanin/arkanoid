﻿using UnityEngine;

public class AutoPlatformMovement : APlatformMovement
{
	protected override Vector3 CalculateVelocity()
	{
		var ball = GetClosestBall();

		if (ball == null)
		{
			return Vector3.zero;
		}

		var ballRb = ball.GetComponent<Rigidbody>();

		if (ballRb != null && !ballRb.isKinematic)
		{
			var axis = Mathf.Clamp(ball.transform.position.x - transform.position.x, -1, 1);
			return Vector3.right * m_movementSpeed.Value * axis;
		}

		return Vector3.zero;

	}

	private Transform GetClosestBall()
	{
		var balls = GameObject.FindGameObjectsWithTag("Ball");
		Transform closestBall = null;

		float closestSqrDistance = float.MaxValue;

		foreach (var ball in balls)
		{
			var distance = (ball.transform.position - transform.position).sqrMagnitude;

			if (distance < closestSqrDistance)
			{
				closestBall = ball.transform;
				closestSqrDistance = distance;
			}
		}

		return closestBall;
	}
}
