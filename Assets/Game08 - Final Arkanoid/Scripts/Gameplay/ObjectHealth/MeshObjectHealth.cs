﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshObjectHealth : ObjectHealth
{
    [SerializeField]
    private Mesh[] m_meshes = null;

    private MeshFilter m_meshFilter;

    protected override void Start()
    {
        base.Start();

        m_meshFilter = GetComponent<MeshFilter>();
        SetMesh();
    }

    private void SetMesh()
    {
        if (m_meshes.Length > 0)
        {
            int index = m_meshes.Length - (int)Health;

            if (index >= 0 && index < m_meshes.Length)
            {
                m_meshFilter.mesh = m_meshes[index];
            }
        }
    }

    protected override void OnHealthChanged()
    {
        base.OnHealthChanged();

        SetMesh();
    }
}
