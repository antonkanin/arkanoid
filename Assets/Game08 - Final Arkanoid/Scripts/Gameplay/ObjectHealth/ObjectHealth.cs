﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class ObjectHealth : MonoBehaviour
{
    [SerializeField]
    private float m_health = 1f;

    public UnityEvent OnDieEvent = new UnityEvent();

    public delegate void HealthChangedEventHandler();

    public event HealthChangedEventHandler OnHealthChangedEvent;

    private Rigidbody m_rb;

    private float m_prevHealth;

    public float Health
    {
        get { return m_health; }
        set { m_health = value; }
    }

    protected virtual void Start()
    {
        m_prevHealth = Health;
        m_rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (transform.position.y > 1f)
        {
            m_rb.WakeUp();
        }

        if (m_prevHealth == Health)
        {
            return;
        }

        OnHealthChanged();

        if (Health <= 0)
        {
            Destroy(gameObject);
            OnDieEvent.Invoke();
        }
    }

    protected virtual void OnHealthChanged()
    {
        if (OnHealthChangedEvent != null)
        {
            OnHealthChangedEvent.Invoke();
        }
    }
}
