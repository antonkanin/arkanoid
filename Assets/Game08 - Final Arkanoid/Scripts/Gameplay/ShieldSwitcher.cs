﻿using UnityEngine;

public class ShieldSwitcher : MonoBehaviour
{
	[Header("Base shield: ")]
	[SerializeField] private GameObject m_shield = null;

	[Header("Big shield:")]
	[SerializeField] private GameObject m_bigShield = null;

	private void Start()
	{
		SwitchShield(false);
	}

	public void SwitchShield(bool isBig)
	{
		m_shield.gameObject.SetActive(!isBig);
		m_bigShield.gameObject.SetActive(isBig);
	}
}
