﻿using UnityEngine;

public delegate void OnDestroyHandler(int index);

public class WallDestroyCallBack : MonoBehaviour
{
    public int WallIndex { set; get; }

    public event OnDestroyHandler WallDestroyed;

    private void OnDestroy()
    {
        if (WallDestroyed != null)
        {
            WallDestroyed(WallIndex);
        }
    }
}
