﻿using UnityEngine;

public class FingerInput : IInput
{
    private float m_mouseDownX;
    private bool m_isMouseDown = false;
    private float m_previousPosition;

    [SerializeField]
    private float m_swipeDelta = 0.1f;

    public void UpdateInput()
    {
        CustomInput.HorizontalAxis = 0;
        if (Input.GetMouseButton(0))
        {
            if (m_isMouseDown == false)
            {
                m_mouseDownX = Input.mousePosition.x;
                m_isMouseDown = true;
            }
            else
            {
                if (Mathf.Abs(Input.mousePosition.x - m_mouseDownX) >= m_swipeDelta)
                {
                    CustomInput.HorizontalAxis = (Input.mousePosition.x - m_previousPosition) / Time.deltaTime / 1000;
                }

                m_previousPosition = Input.mousePosition.x;
            }
        }
        else
        {
            m_isMouseDown = false;
        }
    }
}
