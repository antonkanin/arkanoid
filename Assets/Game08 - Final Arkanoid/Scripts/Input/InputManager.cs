﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
	public delegate void GameStartEventHandler();

	[SerializeField] private InputSettings m_settings = null;

	private IInput m_input;

	private void Start()
	{
		SetInput();

		m_settings.OnInputChanged.AddListener(SetInput);
	}

	private void OnDestroy()
	{
		m_settings.OnInputChanged.RemoveListener(SetInput);
	}

	private void Update()
	{
		if (m_input != null)
		{
			m_input.UpdateInput();
		}
	}

	private void SetInput()
	{
		switch (m_settings.CurrType)
		{
			case EInputType.Keyboard:
				m_input = new KeyboardInput();
				break;
			case EInputType.Accelerometer:
				m_input = new MobileInput();
				break;
            case EInputType.Finger:
                m_input = new FingerInput();
                break;
			default:
				m_input = null;
				break;
		}
	}
}
