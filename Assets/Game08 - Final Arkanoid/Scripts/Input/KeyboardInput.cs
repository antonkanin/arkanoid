﻿using UnityEngine;

public class KeyboardInput : IInput
{
	public void UpdateInput()
	{
		CustomInput.HorizontalAxis = 0;

		if (Input.GetKey(KeyCode.LeftArrow))
		{
			CustomInput.HorizontalAxis = -1;
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
		    CustomInput.HorizontalAxis = 1;
		}
		else if (Input.GetKey(KeyCode.Space))
		{
		    GameController.StartGame();
		}
    }
}
