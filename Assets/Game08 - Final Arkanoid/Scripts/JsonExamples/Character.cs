﻿using System.Net;
using UnityEngine;

public enum EClass
{
	Warrior = 0,
	Mage = 1,
	Paladin = 2
}

public class Character
{
	public string Name;
	public int Health;
	public int Level;
	public float Experience;
	public EClass Class;

	public string Test { get; set; }
}
