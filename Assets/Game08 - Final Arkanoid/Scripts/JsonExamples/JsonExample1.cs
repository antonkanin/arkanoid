﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class JsonExample1 : MonoBehaviour
{
	void Start()
	{
		var character1 = new Character()
		{
			Name = "Anduin",
			Health = 160,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var json = JsonConvert.SerializeObject(character1);
		Debug.Log(json);
	}

	// Update is called once per frame
	void Update()
{

}
}
