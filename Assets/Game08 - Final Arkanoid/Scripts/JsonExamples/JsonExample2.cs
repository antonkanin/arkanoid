﻿using Newtonsoft.Json;
using UnityEngine;

public class JsonExample2 : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		var character1 = new Character()
		{
			Name = "Anduin",
			Health = 160,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var character2 = new Character()
		{
			Name = "Thor",
			Health = 160,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var character3 = new Character()
		{
			Name = "Gimli",
			Health = 300,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var characters = new Character[]
		{
			character1,
			character2,
			character3
		};

		var json = JsonConvert.SerializeObject(characters);
		Debug.Log(json);
	}
}
