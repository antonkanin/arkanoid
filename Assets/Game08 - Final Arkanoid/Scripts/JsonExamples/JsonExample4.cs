﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;

public class JsonExample4 : MonoBehaviour
{
	void Start()
	{
		var character1 = new Character()
		{
			Name = "Anduin",
			Health = 160,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var character2 = new Character()
		{
			Name = "Thor",
			Health = 160,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var character3 = new Character()
		{
			Name = "Gimli",
			Health = 300,
			Level = 3,
			Experience = 156.78f,
			Class = EClass.Mage
		};

		var characters = new Dictionary<string, Character>();
		characters.Add("Character1", character1);
		characters.Add("Character2", character2);
		characters.Add("Character3", character3);

		var json = JsonConvert.SerializeObject(characters);
		var path = Application.persistentDataPath + "/Characters.json";

		File.WriteAllText(path, json);
		Debug.Log(json);
		Debug.Log(path
		);
	}
}
