﻿using UnityEngine;

public class LevelBtnPlacement : MonoBehaviour
{
	[SerializeField] private LoadLevelBtn m_btnPrefab;
	[SerializeField] private Transform m_content;
	[SerializeField] private int m_numOfButtons;

	[ContextMenu("PlaceButtons")]
	public void PlaceButtons()
	{
		while (m_content.childCount > 0)
		{
			DestroyImmediate(m_content.GetChild(0).gameObject);
		}

		for (int i = 0; i < m_numOfButtons; i++)
		{
			var instance = Instantiate(m_btnPrefab, m_content);
			instance.LevelNum = i + 1;
			instance.SetLevelNumTmp(instance.LevelNum);
			instance.SetStars(StarsStorage.GetStars(instance.LevelNum));
			instance.name = "LevelBtn_" + instance.LevelNum;

			instance.gameObject.SetActive(true);
		}
	}
}
