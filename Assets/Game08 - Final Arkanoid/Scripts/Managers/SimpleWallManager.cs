﻿using UnityEngine;

public class SimpleWallManager : MonoBehaviour
{
    private int m_wallIndex = 0;

    public void EnableNextWall()
    {
        int counter = 0;
        foreach (Transform wallTransform in transform)
        {
            if (counter == m_wallIndex)
            {
                wallTransform.gameObject.SetActive(true);
                m_wallIndex++;
                break;
            }
            counter++;
        }
    }
}
