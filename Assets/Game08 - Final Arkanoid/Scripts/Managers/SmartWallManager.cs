﻿using UnityEngine;
using System.Collections;

public class SmartWallManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_startPositions = null;
    [SerializeField]
    private WallTemplate m_woodenWall = null;
    [SerializeField]
    private WallTemplate m_stoneWall = null;
    [SerializeField]
    private WallTemplate m_steelWall = null;

	[SerializeField]
	private float m_delay = 1f;

	private bool[] m_wallsPlacement;

    void Start()
    {
        m_wallsPlacement = new bool[m_startPositions.Length];
    }

    public void AddSteelWall()
    {
        AddWall(m_steelWall);
    }

    public void AddStoneWall()
    {
        AddWall(m_stoneWall);
    }

    public void AddWoodWall()
    {
        AddWall(m_woodenWall);
    }

	public void AddMetalFullWallAsync()
	{
		StartCoroutine(AddFullWallAsync(m_steelWall));
	}

	public void AddWoodFullWallAsync()
	{
		StartCoroutine(AddFullWallAsync(m_woodenWall));
	}

	public void AddStoneFullWallAsync()
	{
		StartCoroutine(AddFullWallAsync(m_stoneWall));
	}

	IEnumerator AddFullWallAsync(WallTemplate wallTemplate)
	{
		for (int i = 0; i < m_wallsPlacement.Length; i++)
		{
			if (m_wallsPlacement[i] == false)
			{
				var wallPrafab = wallTemplate.WallPrefabs[i];
				var wall = Instantiate(wallPrafab,
					m_startPositions[i].transform.position,
					m_startPositions[i].transform.rotation,
					transform);

				wall.GetComponent<WallDestroyCallBack>().WallIndex = i;
				wall.GetComponent<WallDestroyCallBack>().WallDestroyed += OnWallDestroy;

				m_wallsPlacement[i] = true;
				yield return new WaitForSeconds(m_delay);
			}
		}
	}

    private void AddWall(WallTemplate wallTemplate)
    {
        for (int i = 0; i < m_wallsPlacement.Length; i++)
        {
            if (m_wallsPlacement[i] == false)
            {
                var wallPrafab = wallTemplate.WallPrefabs[i];
                var wall = Instantiate(wallPrafab,
                    m_startPositions[i].transform.position,
                    m_startPositions[i].transform.rotation,
                    transform);

                wall.GetComponent<WallDestroyCallBack>().WallIndex = i;
                wall.GetComponent<WallDestroyCallBack>().WallDestroyed += OnWallDestroy;
                
                m_wallsPlacement[i] = true;

                break;
            }
        }
    }

    public void OnWallDestroy(int index)
    {
        m_wallsPlacement[index] = false;
    }
}
