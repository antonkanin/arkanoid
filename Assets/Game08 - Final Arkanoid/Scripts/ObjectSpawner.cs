﻿using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_prefabs = null;

    public void Spawn()
    {
        foreach (var prefab in m_prefabs)
        {
            Instantiate(prefab, transform.position, transform.rotation);
        }
    }
}