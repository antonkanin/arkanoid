﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadLevel : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Ball"))
		{
			if (GetBallsCount() > 1)
			{
				Destroy(other.gameObject);
			}
			else
			{
				GameController.PlayerHealth--;
				GameController.IsPaused = true;
			}
		}
	}

	private int GetBallsCount()
	{
		var balls = GameObject.FindGameObjectsWithTag("Ball");
		return balls.Length;
	}
}