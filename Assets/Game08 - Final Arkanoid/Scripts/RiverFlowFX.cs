﻿using UnityEngine;

public class RiverFlowFX : MonoBehaviour
{
    [SerializeField] private MeshRenderer m_renderer = null;
    [SerializeField] private float m_flowSpeed = 1.0f;

    private float m_currOffset;

    void Update()
    {
        m_currOffset += Time.deltaTime * m_flowSpeed;
        m_renderer.material.SetTextureOffset("_MainTex", new Vector2(m_currOffset, 0));
    }
}
