﻿using System;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class SaveLoad
{
	public static void Save<T>(string key, T obj)
	{
		try
		{
			var json = JsonConvert.SerializeObject(obj);
			var path = string.Format("{0}/{1}.json", Application.persistentDataPath, key);
			File.WriteAllText(path, json);
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			throw e;
		}
	}

	public static T Load<T>(string key)
	{
		var path = string.Format("{0}/{1}.json", Application.persistentDataPath, key);
		if (File.Exists(path))
		{
			var json = File.ReadAllText(path);
			return JsonConvert.DeserializeObject<T>(json);
		}

		return default(T);
	}
}
