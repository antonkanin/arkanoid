﻿using UnityEngine;

[CreateAssetMenu]
public class StarsStorage : ScriptableObject
{
	[SerializeField] private int m_levelNum;
	[SerializeField] private int m_starsNum;

	private static string m_key = "LevelStars";

	public static int GetStars(int levelNum)
	{
		return PlayerPrefs.GetInt(m_key + levelNum);
	}

	public static void SetStars(int levelNum, int starsCount)
	{
		PlayerPrefs.SetInt(m_key + levelNum, starsCount);
	}

	[ContextMenu("Set stars")]
	public void SetStart()
	{
		SetStars(m_levelNum, m_starsNum);
		Debug.Log("Set stars(), current count: " + GetStars(m_levelNum));
	}
}
