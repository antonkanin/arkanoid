﻿using UnityEngine;
using UnityEngine.Events;

public class ValueChangeEvent : UnityEvent<int> {}

[CreateAssetMenu]
public class IntVariable : ScriptableObject
{
    [SerializeField] private int m_value;

    public ValueChangeEvent OnValueChanged = new ValueChangeEvent();

    public int Value
    {
        get { return m_value; }
        set
        {
            m_value = value;
            OnValueChanged.Invoke(m_value);
        }
    }
}
