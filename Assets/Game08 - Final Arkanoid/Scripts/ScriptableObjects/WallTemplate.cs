﻿using UnityEngine;

[CreateAssetMenu]
public class WallTemplate : ScriptableObject
{
    public GameObject[] WallPrefabs;
}
