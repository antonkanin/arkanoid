﻿using UnityEngine;

public class SkeletonMovement : MonoBehaviour
{
    [SerializeField] private float m_movementSpeed = 1f;
    [SerializeField] private float m_raycastLength = 1f;

    private Animation m_animation;

    void Start()
    {
        m_animation = GetComponent<Animation>();
    }

    void Update()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.back);

        if (Physics.Raycast(transform.position, fwd, m_raycastLength))
        {
            m_animation.Stop();
        }
        else
        {
            transform.Translate(Vector3.back * Time.deltaTime * m_movementSpeed);
            if (!m_animation.isPlaying)
            {
                m_animation.Play();
            }
        }
    }
}
