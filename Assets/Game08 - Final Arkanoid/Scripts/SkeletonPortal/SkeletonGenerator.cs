﻿using System.Collections;
using UnityEngine;

public class SkeletonGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_skeletonPrefab = null;
    [SerializeField] private Transform m_spawnPoint = null;
    [SerializeField] private GameObject m_spawnFX = null;
    [SerializeField] private float m_spawnTimeInterval = 3f;
    [SerializeField] private float m_firstSpawnTimeout = 1f;

    void Start()
    {
        StartCoroutine(InitializeSpawner());
    }

    IEnumerator InitializeSpawner()
    {
        yield return new WaitForSeconds(m_firstSpawnTimeout);
        StartCoroutine(SpawnSkeleton());
        yield return null;
    }

    IEnumerator SpawnSkeleton()
    {
		var delay = new WaitForSeconds(m_spawnTimeInterval);
		while (true)
        {
            var fx = Instantiate(m_spawnFX, m_spawnPoint.position, m_spawnPoint.rotation);
            Destroy(fx, 1.0f);
            Instantiate(m_skeletonPrefab, m_spawnPoint.position, m_spawnPoint.rotation);
            yield return delay;
        }
    }
}
