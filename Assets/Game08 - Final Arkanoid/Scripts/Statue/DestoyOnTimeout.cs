﻿using UnityEngine;

public class DestoyOnTimeout : MonoBehaviour
{
    [SerializeField] private float m_timeout = 5f;

    // Update is called once per frame
    void Update()
    {
        m_timeout -= Time.deltaTime;

        if (m_timeout <= 0)
        {
            Destroy(gameObject);
        }
    }
}
