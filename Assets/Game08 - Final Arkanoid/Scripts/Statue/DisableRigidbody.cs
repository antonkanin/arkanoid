﻿using UnityEngine;

public class DisableRigidbody : MonoBehaviour
{
    [SerializeField] public float m_timeout = 1f;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void Update ()
	{
	    m_timeout -= Time.deltaTime;

	    if (m_timeout <= 0 && rb.detectCollisions)
	    {
	        rb.detectCollisions = false;
	    }
	}
}
