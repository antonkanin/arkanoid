﻿using UnityEngine;

public class StatueExplosion : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        var rb_list = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rb_list)
        {
            Debug.Log("Shooting pieces");
            rb.AddForce((Vector3.up + Vector3.forward) * 1000);
        }
    }
}
