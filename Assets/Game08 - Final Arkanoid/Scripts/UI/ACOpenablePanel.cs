﻿using UnityEngine;

public class ACOpenablePanel : AOpenablePanel
{
	[SerializeField] public Animator m_animator = null;

    public void OpenClose()
    {
        if (IsOpened)
        {
            Close();
        }
        else
        {
            Open();
        }
    }

    protected override void OpenAnim()
    {
        if (!IsOpened)
        {
            m_animator.SetTrigger("Open");
        }
    }

    protected override void CloseAnim()
    {
        if (IsOpened)
        {
            m_animator.SetTrigger("Close");
        }
    }
}
