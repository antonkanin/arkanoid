﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
public class AchievementViewItem : MonoBehaviour
{
	[SerializeField] private AchivementAsset m_asset;

	[Header("References:")]
	[SerializeField] private TMP_Text m_name;
	[SerializeField] private TMP_Text m_description;
	[SerializeField] private TMP_Text m_progress;
	[SerializeField] private Image[] m_stars;

	void Start()
	{
		TryApplyAsset();
	}

#if UNITY_EDITOR
	void Update()
	{
		TryApplyAsset();
	}
#endif

	private void TryApplyAsset()
	{
		if (m_asset == null)
		{
			return;
		}

		SetName(m_asset.Name);
		SetDescription(m_asset.Description);
		SetProgress(m_asset.GetProgressSting());
		SetNumOfStars(m_asset.ProgressSteps.Length);
		SetCompletedStars(m_asset.CompletedStars);
	}

	private void SetName(string name)
	{
		if (m_name != null)
		{
			m_name.text = name;
		}
	}

	private void SetDescription(string description)
	{
		if (m_description != null)
		{
			m_description.text = description;
		}
	}

	private void SetProgress(string progress)
	{
		if (m_progress != null)
		{
			m_description.text = progress;
		}
	}

	private void SetNumOfStars(int num)
	{
		for (int i = 0; i < m_stars.Length; i++)
		{
			m_stars[i].gameObject.SetActive(i < num);
		}
	}

	private void SetCompletedStars(int stars)
	{
		var activeStar = Resources.Load<Sprite>("Stars/activeStar");
		var inactiveStar = Resources.Load<Sprite>("Stars/inactiveStar");

		for (int i = 0; i < m_stars.Length; i++)
		{
			if (m_stars[i].gameObject.activeSelf)
			{
				m_stars[i].sprite = i < stars ? activeStar : inactiveStar;
			}
		}
	}
}
