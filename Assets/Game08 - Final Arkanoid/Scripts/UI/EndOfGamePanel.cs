﻿using UnityEngine;
using UnityEngine.UI;

public class EndOfGamePanel : MonoBehaviour
{
	[SerializeField] private Animator m_animator = null;
	[SerializeField] private GameObject m_winDialog = null;
	[SerializeField] private GameObject m_loseDialog = null;
	[SerializeField] private GameObject m_shadow = null;

	// Use this for initialization
	private void Start()
	{
		m_shadow.SetActive(false);
		m_winDialog.SetActive(false);
	    m_loseDialog.SetActive(false);

		GameController.EndOfGameEvent += OnEndOfGame;
	}

	private void OnDestroy()
	{
		GameController.EndOfGameEvent -= OnEndOfGame;
	}

	private void OnEndOfGame(bool isWin)
	{
		GameController.EndOfGameEvent -= OnEndOfGame;

		m_animator.SetTrigger("Open");
		m_winDialog.SetActive(isWin);
		m_loseDialog.SetActive(!isWin);
	    m_shadow.SetActive(true);
    }
}
