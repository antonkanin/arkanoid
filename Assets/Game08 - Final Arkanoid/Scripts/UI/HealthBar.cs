﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image[] m_healthImages = null;

    void Start()
    {
        SetHealthBar(GameController.PlayerHealth);
    }

    void Update()
    {
        SetHealthBar(GameController.PlayerHealth);
    }

    void SetHealthBar(int health)
    {
        for (int index = 0; index < m_healthImages.Length; index++)
        {
            m_healthImages[index].enabled = index < health;
        }
    }
}
