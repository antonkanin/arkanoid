﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TMP_Dropdown))]
public class InputOptionsDropdown : MonoBehaviour
{
    [SerializeField] private InputSettings m_inputSettings = null;

    private void Start()
    {
        string[] inputTypes = System.Enum.GetNames(typeof(EInputType));
        GetComponent<TMP_Dropdown>().AddOptions(new List<string>(inputTypes));
        GetComponent<TMP_Dropdown>().value = (int)m_inputSettings.CurrType;
    }

    public void OnInputTypeChanged(int code)
    {
        Debug.Log("Changing type to: " + (EInputType)code);
        m_inputSettings.CurrType = (EInputType)code;
    }
}
