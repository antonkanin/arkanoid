﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

[RequireComponent((typeof(Button)))]
public class LoadLevelBtn : MonoBehaviour
{
	public int LevelNum;

	[SerializeField] private TMP_Text m_levelNum;
	[SerializeField] private Image[] m_stars;

	private void Start()
	{
		SetStars(StarsStorage.GetStars(LevelNum));
		SetLevelNumTmp(LevelNum);

		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	private void OnDestroy()
	{
		GetComponent<Button>().onClick.RemoveListener(OnClick);
	}

	public void SetStars(int num)
	{
		var activeStar = Resources.Load<Sprite>("Stars/activeStar");
		var inactiveStar = Resources.Load<Sprite>("Stars/inactiveStar");

		for (int i = 0; i < m_stars.Length; i++)
		{
			m_stars[i].sprite = i < num ? activeStar : inactiveStar;
		}
	}

	public void SetLevelNumTmp(int levelNum)
	{
		if (m_levelNum != null)
		{
			m_levelNum.text = LevelNum.ToString();
		}
	}

	private void OnClick()
	{
		if (LevelNum > 0 && LevelNum < SceneManager.sceneCountInBuildSettings - 1)
		{
			SceneManager.LoadSceneAsync(LevelNum);
		}
	}
}
