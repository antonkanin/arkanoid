﻿using UnityEngine;
using TMPro;

public class MoneyDisplay : MonoBehaviour
{
	[SerializeField] private TMP_Text m_amountText;

	void Start()
	{
		UpdateMoneyAmount(ECurrency.Coin);
		UserWallet.OnItemChanged += UpdateMoneyAmount;
	}

	private void OnDestroy()
	{
		UserWallet.OnItemChanged -= UpdateMoneyAmount;
	}

	private void UpdateMoneyAmount(ECurrency currency)
	{
		if (currency == ECurrency.Coin)
		{
			var amount = UserWallet.GetValue(currency);
			m_amountText.text = "Coins: " + amount;
		}
	}
}
