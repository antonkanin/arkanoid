﻿using UnityEngine;
using UnityEngine.UI;

public class StartsCounter : MonoBehaviour
{
    [SerializeField]
    private Image[] m_images;

    // Use this for initialization
    void Start()
    {
        UpdateImages(0);
        GameController.OnStarsCountUpdate.AddListener(UpdateImages);
    }

    void OnDestroy()
    {
        GameController.OnStarsCountUpdate.RemoveListener(UpdateImages);
    }

    private void UpdateImages(int imagesCount)
    {
        for (int i = 0; i < m_images.Length; i++)
        {
            m_images[i].enabled = i < imagesCount;
        }
    }
}
