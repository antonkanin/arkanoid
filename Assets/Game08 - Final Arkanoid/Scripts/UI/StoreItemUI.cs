﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

[ExecuteInEditMode]
public class StoreItemUI : MonoBehaviour
{
	[SerializeField] private StoreItem m_asset;

	[Header("References:")]
	[SerializeField] private TMP_Text m_nameTxt;
	[SerializeField] private TMP_Text m_amountTxt;
	[SerializeField] private TMP_Text m_priceTxt;
	[SerializeField] private Image m_icon;
	[SerializeField] private Button m_buyBtn;

	private void Start()
	{
		if (m_buyBtn != null)
		{
			m_buyBtn.onClick.AddListener(OnClick);
		}
	}

	private void OnDestroy()
	{
		if (m_buyBtn != null)
		{
			m_buyBtn.onClick.RemoveListener(OnClick);
		}
	}

	private void Update()
	{
		if (m_asset != null)
		{
			ApplyAsset();
		}
	}

	private void ApplyAsset()
	{
		m_nameTxt.text = m_asset.MName;
		m_amountTxt.text = "+" + m_asset.MGoodAmount.ToString();
		m_priceTxt.text = "$" + m_asset.MPrice.ToString();
		m_icon.sprite = m_asset.MIcon;
	}

	private void OnClick()
	{
		if (m_asset != null)
		{
			m_asset.Buy();
		}
	}

	private void Buy()
	{
		var amount = UserWallet.GetValue(m_asset.MGoodCurrency);
		amount += m_asset.MGoodAmount;
		UserWallet.SetValue(m_asset.MGoodCurrency, amount);		
	}
}
