﻿using UnityEngine;

[RequireComponent(typeof(ObjectHealth))]
public class WallHitFX : MonoBehaviour
{
    [SerializeField]
    private GameObject m_PrefabFX = null;

    private ContactPoint cp;

    private void OnCollisionEnter(Collision other)
    {
        foreach (var p in other.contacts)
        {
            GameObject fx = Instantiate(m_PrefabFX, p.point, Quaternion.Euler(p.normal));
            Destroy(fx, 5f);

            Debug.Log("Collision at: " + p.point + " with normal " + p.normal);
            cp = p;
        }
    }

    void Update()
    {
        Debug.DrawRay(cp.point, 10f * cp.normal, Color.yellow, 3f);
    }
}
