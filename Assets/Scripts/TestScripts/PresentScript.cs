﻿using UnityEngine;
using System.Collections;

public class PresentScript : MonoBehaviour {
	string FirstName = "Даша";
	int FirstAge = 25;
	bool isFirstMale = false;
	
	string SecondName = "Георгий";
	int SecondAge = 22;
	bool isSecondMale = true;
	
	// Use this for initialization
	void Start () {
		
		string name = FirstName;
		int age = FirstAge;
		bool isMale = isFirstMale;
		
		
		if (SecondAge > FirstAge) {
			name = SecondName;
			age = SecondAge;
			isMale = isSecondMale;
		}
		
		string present = "цветы";
		if (age >= 30 && !isMale || isMale && age < 30) present = "конфеты";
		
		if (isMale && age >= 30) present = "бритву";
		
		print("Я подарил человеку с именем " + name + " в подарок " + present);
		
	}
}
