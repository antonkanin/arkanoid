https://simonschreibt.de/

Oct/17/2018
  Add a parameter if bonus can timeout or not
  Make protection from two 'same' effects onto the same platform
  Make a 'slowdown' bones, use chain prefab instead of 
  Make bonus 'small shield', only when big shield is active
  When the barrel gets destroyed add ability to generate multiple bonuses

Oct/15/2018 - Homework
  Change setactive to instantiate
  Add side walls (left and right)
  Build walls one after another

Oct/15/2018
  Service locator
  How to read texture with high performance

Oct/10/2018
  Skinned Mesh Renderer

Oct/04/2018
  Quesiton - what's the best pattern to process level change, events?

Oct/03/2018
  Static Batching in Unity

Sep/24/2018
  ConsolePro

Sep/17/2018
  mip maps
  anisotropic filtering

Homework
1. Shooter 
  - add two more balls
  - add two more bullets, fast/slow
  - script that will remove the ball in 5 seconds